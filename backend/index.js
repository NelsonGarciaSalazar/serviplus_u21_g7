let app = require('./src/app');
let connection = require('./src/db/connection');

const PORT = 5000;

app.listen( PORT, ()=> {
    console.log("Server ready on port", PORT);
});