const { Router } = require('express');
const userController = require('../controllers/userController');
const authController = require('../controllers/authController');

let userRouter = Router();

// users
userRouter.post( '/user/save' , authController.verifyToken, userController.saveUser );
userRouter.get( '/user/list/:search?', userController.listUsers );
userRouter.get( '/user/:id', userController.findUser );
userRouter.put( '/user/:id', authController.verifyToken, userController.updateUser );
userRouter.delete( '/user/:id', authController.verifyToken, userController.deleteUser );

// Auth
userRouter.post('/auth/login', authController.login);
userRouter.post('/auth/test', authController.verifyToken, authController.test);

module.exports = userRouter;