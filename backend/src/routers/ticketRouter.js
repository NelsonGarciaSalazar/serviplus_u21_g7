const { Router } = require('express');
const ticketController = require('../controllers/ticketController');

let ticketRouter = Router();

// tickets
ticketRouter.post( '/ticket/save' , ticketController.saveTicket );
ticketRouter.get( '/ticket/list/:search?', ticketController.listTickets );
ticketRouter.get( '/ticket/:id', ticketController.findTicket );
ticketRouter.put( '/ticket/:id', ticketController.updateTicket );
ticketRouter.delete( '/ticket/:id',ticketController.deleteTicket );

module.exports = ticketRouter;