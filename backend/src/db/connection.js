const mongoose = require('mongoose');

mongoose.connect("mongodb://localhost:27017/serviplus", { }, (err, res) =>{
    if(err){
        console.log(err);
    }else{
        console.log("Connection successful");
    }
});

module.exports = mongoose;