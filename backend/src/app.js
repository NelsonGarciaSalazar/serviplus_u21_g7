const express = require('express');
const userRouter = require('./routers/userRouter');
const ticketRouter = require('./routers/ticketRouter');

let app = express();

// Add middleware
// express.json Esta librería permite que las solicitudes se puedan convertir en Json
 
app.use( express.json() );
app.use( express.urlencoded( {extended:true}) );

//express. urlencode:Esta permite analizar que viene en los parametros ()urñ, parametro, valor


// Para todas las solicitudes y respuestas haga lo siguiente (permisos de encabezados y solicitudes de varias fuentes y metodos de accesa)
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*'); 
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, XRequested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS');
    res.header('Allow', 'GET, POST, PUT, PATCH, DELETE, OPTIONS');
    next();
});

// Add routers
app.use ( userRouter, ticketRouter );

module.exports = app;