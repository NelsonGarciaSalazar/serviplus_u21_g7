const jwt =  require( "jsonwebtoken" );
const User = require('../models/user');

function login( req, res ){
    
    const user = "Admin";

    jwt.sign( user, "secretkey", (  err, token) => {
        res.send(
            {
                token: token
            }
        )
    });
}

function test( req, res ){
    res.status(200).send({ testResult: req.data });
}

// Function called before access the route
function verifyToken( req, res, next ){
    const requestHeader = req.headers['authorization'];

    if (typeof requestHeader !== 'undefined') {
        
        const token = requestHeader.split(" ")[1];

        jwt.verify( token, 'secretkey', (err, data) => {
                if (err) {
                    res.sendStatus(403);
                }else{
                    req.data = data;
                    next();
                }
            } 
        );
    
    }else{
        res.sendStatus(403);
    }
}

module.exports = { login, verifyToken, test };