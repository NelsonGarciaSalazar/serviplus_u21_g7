let Ticket = require('../models/ticket');

function saveTicket( req, res ){

    let myTicket = new Ticket( req.body );
    
    myTicket.save( ( err, result ) => {
        if(err){
            res.status(500).send( { message: err } );
        }else{
            res.status(200).send( { message: result });
        }
    });
}

function listTickets( req, res){

    let search = req.params.search;
    
    let queryParam = {};

    if( search ){
        
        queryParam = { 
            $or : [
                { ticketNumber: { $regex: search, $options: "i"  } },
                { mail: { $regex: search, $options: "i"  } },
                { titule: { $regex: search, $options: "i"  } },
                { category: { $regex: search, $options: "i"  } },
                { status: { $regex: search, $options: "i"  } }                
            ]
        };
    }

    let query = Ticket.find( queryParam ).sort('createdAt');

    
    query.exec( (err, result) =>{

        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    });

}

function findTicket( req, res ) {
    
    let id = req.params.id;
    let query = Ticket.findById(id);

    query.exec( ( err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    });

}

function updateTicket( req, res ){

    let id = req.params.id;
    let data = req.body;
    
    Ticket.findByIdAndUpdate( id, data, { new: true }, (err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    } );
}

function deleteTicket( req, res ){

    let id = req.params.id;

    Ticket.findByIdAndDelete( id, (err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send(  { message: "Ticket deleted", "result": result } );
        }
    } );    
}

module.exports = { saveTicket, listTickets,findTicket, updateTicket, deleteTicket };
