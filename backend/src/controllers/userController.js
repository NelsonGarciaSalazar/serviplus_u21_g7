let User = require('../models/user');

function saveUser( req, res ){

    let myUser = new User( req.body );
    myUser.user = req.data.mail;

    myUser.save( ( err, result ) => {
        if(err){
            res.status(500).send( { message: err } );
        }else{
            res.status(200).send( { message: result });
        }
    });
}

function listUsers( req, res){

    let search = req.params.search;
    
    let queryParam = {};

    if( search ){
        
        queryParam = { 
            $or : [
                { mail: { $regex: search, $options: "i"  } },
                { firstName: { $regex: search, $options: "i"  } },
                { lastName: { $regex: search, $options: "i"  } },
                { rol: { $regex: search, $options: "i"  } }
            ] 
        };
    }

    let query = User.find( queryParam ).sort('firstName');
    
    query.exec( (err, result) =>{

        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    });

}

function findUser( req, res ) {
    
    let id = req.params.id;
    let query = User.findById(id);

    query.exec( ( err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    });

}


function updateUser( req, res ){

    let id = req.params.id;
    let data = req.body;

    User.findByIdAndUpdate( id, data, { new: true }, (err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    } );

}

function deleteUser( req, res ){

    let id = req.params.id;

    User.findByIdAndDelete( id, (err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    } );
    
}

module.exports = { saveUser, listUsers,findUser, updateUser, deleteUser };
