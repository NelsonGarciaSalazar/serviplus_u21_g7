const mongoose = require('mongoose');
let Schema = mongoose.Schema;

const ticketSchema = Schema(
    {
        ticketNumber: {type: String, require: true},
        mail: {type: String, require: true},
        titule: {type: String, require: true},
        category: {type: String, require: true},
        status: {type: String, require: true},
        replys: [
                    {
                        mail: {type: String, require: true},
                        review: {type: String, require: true}
                    }
                ]
    },
    {
        timestamps: true
    }
);

const Tickect = mongoose.model('tickets', ticketSchema);

module.exports = Tickect