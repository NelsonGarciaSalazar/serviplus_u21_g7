const mongoose = require('mongoose');

let Schema = mongoose.Schema;

const userSchema = Schema(
    {
        mail: { type: String, required: true },
        firstName: { type: String, required: true },
        lastName: { type: String, required: true },
        rol: { type: String, required: true},
        password: { type: String, required: true }
    }
);

const User = mongoose.model('users', userSchema);
module.exports = User;