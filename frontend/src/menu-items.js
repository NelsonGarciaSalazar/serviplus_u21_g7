export default {
    items: [
        {
            id: 'navigation',
            title: 'Navigation',
            type: 'group',
            icon: 'icon-navigation',
            children: [
                {
                    id: 'dashboard',
                    title: 'Dashboard',
                    type: 'item',
                    url: '/dashboard/default',
                    icon: 'feather icon-home',
                }
            ]
        },
        {
            id: 'ui-tickets',
            title: 'Tickets',
            type: 'group',
            icon: 'icon-ui',
            children: [
                {
                    id: 'lista_tickets',
                    title: 'Lista Tickets',
                    type: 'item',
                    url: '/dashboard/default',
                    icon: 'feather icon-server',
                }
            ]
        },
    ]
}