import React from 'react';

const Registro = React.lazy(() => import('./Pages/Auth/SignUp/Registro'));
const Login = React.lazy(() => import('./Pages/Auth/SignIn/login'));

const route = [
    { path: '/auth/registro', exact: true, name: 'Registro', component: Registro },
    { path: '/auth/login', exact: true, name: 'Ingreso', component: Login }
];

export default route;